# README #

## What is this repository for? ##

The code in this repository compares a set of more or less general available codes,
based on Brent's zero(), to find a root or zero of a function in a single variable.

## Summary ##

Not all codes claiming to implement Brent's zero() do a good job.
For use in JavaScript, you are adviced to use the package 'brent-zero-generator'.

The number of iterations is a key performance indicator. However, in some cases,
a low number of iterations is the result of a premature ending of the algorithm.
A typical example of this is the function 'x =>x**9', where the package
'brents-method' finds a root at 0.015619 after 12 iterations. Clearly wrong.

## Code origins ##

Most of these are original C language code, which have been transpiled to JavaScript, by hand. The current codes are:

 1. burkardt.js
    from [John Burkardt](https://people.sc.fsu.edu/~jburkardt)'s [BRENT](https://people.sc.fsu.edu/~jburkardt/c_src/brent/brent.html) page. This is essentially Brent's fortran version, transformed to C using the unix utility f2c,
    and manually changing the result to standard C. 
 1. netlib.js  
    Based on the C code from [brent.shar](http://www.netlib.org/c/brent.shar) in the [netlib.org](http://www.netlib.org) collection
 2. recipes.js  
    from the book 'Numerical Recipes 3rd Ed.', converted from C++ to C
 4. wikipedia.js  
    from the talk tab of [Brent's method](https://en.wikipedia.org/wiki/Talk:Brent%27s_method) on Wikipedia.orgsame as BrentsMethod.C with small modifications
 5. npm package brents-method  
    This package claims to implement Brent's zero(), but in version 2.0.1,
    has at least 3 serious bugs, and even when these are fixed, will still have convergence issues.
 6. npm package brent-zero-generator  
    This package was originally create like 'burkardt.js', but manually adjusted
    to closely follow Brent's Algol 60 version of the zero() function.  
    The results should be identical to the results of the 'burkardt.js' code.
   
## Results ##

The output of the program first shows the function and its interval. Then
by code it shows the number of iterations, the root value, and the function
value at this root.

```
1: x=>Math.sqrt(x)-Math.cos(x)     in [0, 1]
     f(0) = -1    f(1) = 0.45969769413186023
burkardt.js            8 0.6417143708728826 0
netlib.js              8 0.6417143708728826 0
recipes.js             8 0.6417143708728826 0
wikipedia.js           8 0.6417143708728826 0
brents-method         36 0.6417143708728824 -3.3306690738754696e-16
brent-zero-generator   8 0.6417143708728826 0

2: x=>Math.pow(x,3)-7*Math.pow(x,2)+14*x-6     in [0, 1]
     f(0) = -6    f(1) = 2
burkardt.js            9 0.5857864376269051 0
netlib.js              9 0.585786437626905 0
recipes.js             9 0.5857864376269051 0
wikipedia.js           9 0.5857864376269051 0
brents-method         10 0.585786437626905 0
brent-zero-generator   9 0.5857864376269051 0

3: x=>Math.pow(x,3)-7*Math.pow(x,2)+14*x-6     in [3.2, 4]
     f(3.2) = -0.11199999999999477    f(4) = 2
burkardt.js           14 3.4142135623730923 0
netlib.js             14 3.4142135623731 0
recipes.js            14 3.4142135623730923 0
wikipedia.js          11 3.4142135623731025 0
brents-method         70 3.4142135623730976 0
brent-zero-generator  14 3.4142135623730923 0

4: x=>Math.pow(x,4)-2*Math.pow(x,3)-4*Math.pow(x,2)+4*x+4     in [-2, -1]
     f(-2) = 12    f(-1) = -1
burkardt.js           10 -1.414213562373095 0
netlib.js             10 -1.414213562373095 0
recipes.js            10 -1.414213562373095 0
wikipedia.js          11 -1.414213562373095 0
brents-method         20 -1.414213562373095 0
brent-zero-generator  10 -1.414213562373095 0

5: x=>Math.pow(x,4)-2*Math.pow(x,3)-4*Math.pow(x,2)+4*x+4     in [0, 2]
     f(0) = 4    f(2) = -4
burkardt.js            9 1.414213562373095 1.7763568394002505e-15
netlib.js              9 1.414213562373095 1.7763568394002505e-15
recipes.js             9 1.414213562373095 1.7763568394002505e-15
wikipedia.js          53 1.414213562373095 1.7763568394002505e-15
brents-method        202 NaN NaN
brent-zero-generator   9 1.414213562373095 1.7763568394002505e-15

6: x=>x - 2 * (-x)     in [0, 1]
     f(0) = 0    f(1) = 3
burkardt.js            2 0 0
netlib.js              2 0 0
recipes.js             2 0 0
wikipedia.js           2 0 0
brents-method          2 0 0
brent-zero-generator   2 0 0

7: x=>Math.exp(x) + (-x+3)*x-2     in [0, 1]
     f(0) = -1    f(1) = 2.7182818284590446
burkardt.js            7 0.25753028543986073 0
netlib.js              7 0.25753028543986073 0
recipes.js             7 0.25753028543986073 0
wikipedia.js           7 0.2575302854398608 0
brents-method         32 0.2575302854398608 0
brent-zero-generator   7 0.25753028543986073 0

8: x=>2*Math.cos(2*x) - (x+1)*(x+1)     in [-1, 0]
     f(-1) = -0.8322936730942848    f(0) = 1
burkardt.js            9 -0.7724522187908169 4.85722573273506e-17
netlib.js              9 -0.7724522187908169 4.85722573273506e-17
recipes.js             9 -0.7724522187908169 4.85722573273506e-17
wikipedia.js          53 -0.7724522187908169 4.85722573273506e-17
brents-method         94 -0.7724522187908169 4.85722573273506e-17
brent-zero-generator   9 -0.7724522187908169 4.85722573273506e-17

9: x=>x+3*Math.cos(x)-Math.exp(x)     in [0, 1]
     f(0) = 2    f(1) = -0.0973749108546258
burkardt.js            8 0.9767730170494437 -4.440892098500626e-16
netlib.js              8 0.9767730170494437 -4.440892098500626e-16
recipes.js             8 0.9767730170494437 -4.440892098500626e-16
wikipedia.js           8 0.9767730170494436 0
brents-method         18 0.9767730170494436 0
brent-zero-generator   8 0.9767730170494437 -4.440892098500626e-16

10: x=>x*x-4*x+4-Math.log(x)     in [1, 2]
     f(1) = 1    f(2) = -0.6931471805599453
burkardt.js           10 1.4123911720238846 -1.1102230246251565e-16
netlib.js             10 1.4123911720238844 0
recipes.js            10 1.4123911720238846 -1.1102230246251565e-16
wikipedia.js          10 1.4123911720238844 0
brents-method         19 1.4123911720238844 0
brent-zero-generator  10 1.4123911720238846 -1.1102230246251565e-16

11: x=>x*x-4*x+4-Math.log(x)     in [2, 4]
     f(2) = -0.6931471805599453    f(4) = 2.613705638880109
burkardt.js           11 3.0571035499947374 -1.5543122344752192e-15
netlib.js             11 3.0571035499947374 -1.5543122344752192e-15
recipes.js            11 3.0571035499947374 -1.5543122344752192e-15
wikipedia.js          10 3.0571035499947383 0
brents-method         58 3.0571035499947383 0
brent-zero-generator  11 3.0571035499947374 -1.5543122344752192e-15

12: x=>x + 1 - 2*Math.sin(Math.PI*x)     in [0, 0.5]
     f(0) = 1    f(0.5) = -0.5
burkardt.js           10 0.20603511957096587 0
netlib.js             10 0.20603511957096587 0
recipes.js            10 0.20603511957096587 0
wikipedia.js          12 0.20603511957096587 0
brents-method         19 0.20603511957096587 0
brent-zero-generator  10 0.20603511957096587 0

13: x=>x + 1 - 2*Math.sin(Math.PI*x)     in [0.5, 1]
     f(0.5) = -0.5    f(1) = 1.9999999999999998
burkardt.js           11 0.6819748087386476 4.440892098500626e-16
netlib.js             11 0.6819748087386476 4.440892098500626e-16
recipes.js            11 0.6819748087386476 4.440892098500626e-16
wikipedia.js          10 0.6819748087386475 0
brents-method          9 0.6819748087386475 0
brent-zero-generator  11 0.6819748087386476 4.440892098500626e-16

14: x=>Math.exp(x) -2 -Math.cos(Math.exp(x)-2)     in [-1, 2]
     f(-1) = -1.570834756180591    f(2) = 4.762858120152077
burkardt.js           12 1.007623971658137 1.1102230246251565e-15
netlib.js             12 1.007623971658137 1.1102230246251565e-15
recipes.js            12 1.007623971658137 1.1102230246251565e-15
wikipedia.js          53 1.0076239716581368 4.440892098500626e-16
brents-method        202 NaN NaN
brent-zero-generator  12 1.007623971658137 1.1102230246251565e-15

15: x=>(x+2)*Math.pow(x+1,2)*x*Math.pow(x-1,3)*(x-2)     in [-0.5, 2.4]
     f(-0.5) = -1.58203125    f(2.4) = 133.98798335999993
burkardt.js           18 0 0
netlib.js             18 0 0
recipes.js            18 0 0
wikipedia.js          15 0 0
brents-method         15 1.308012307565151e-18 5.232049230260604e-18
brent-zero-generator  18 0 0

16: x=>(x+2)*Math.pow(x+1,2)*x*Math.pow(x-1,3)*(x-2)     in [-0.5, 3]
     f(-0.5) = -1.58203125    f(3) = 1920
burkardt.js           16 2 0
netlib.js             16 2 0
recipes.js            16 2 0
wikipedia.js          16 2 0
brents-method         15 2 0
brent-zero-generator  16 2 0

17: x=>(x+2)*Math.pow(x+1,2)*x*Math.pow(x-1,3)*(x-2)     in [-3, -0.5]
     f(-3) = 3840    f(-0.5) = -1.58203125
burkardt.js           14 -1.9999999999999996 -9.592326932761336e-14
netlib.js             14 -1.9999999999999996 -9.592326932761336e-14
recipes.js            14 -1.9999999999999996 -9.592326932761336e-14
wikipedia.js          14 -2 -0
brents-method         28 -2 -0
brent-zero-generator  14 -1.9999999999999996 -9.592326932761336e-14

18: x=>(x+2)*(x+1)*x*Math.pow(x-1,3)*(x-2)     in [-1.5, 1.8]
     f(-1.5) = 20.5078125    f(1.8) = -1.9611647999999997
burkardt.js           16 -1 -0
netlib.js            108 1 -0
recipes.js            16 -1 -0
wikipedia.js          14 -1 -0
brents-method         43 0.999997824519923 6.17754324347895e-17
brent-zero-generator  16 -1 -0

19: x=>Math.pow(x,4)-3*Math.pow(x,2)-3     in [1, 2]
     f(1) = -5    f(2) = 1
burkardt.js            8 1.9471229667070131 0
netlib.js              8 1.9471229667070131 0
recipes.js             8 1.9471229667070131 0
wikipedia.js          11 1.9471229667070131 0
brents-method         28 1.9471229667070131 0
brent-zero-generator   8 1.9471229667070131 0

20: x=>Math.pow(x,3)-x-1     in [1, 2]
     f(1) = -1    f(2) = 5
burkardt.js           10 1.324717957244746 2.220446049250313e-16
netlib.js             11 1.324717957244746 2.220446049250313e-16
recipes.js            10 1.324717957244746 2.220446049250313e-16
wikipedia.js          53 1.324717957244746 2.220446049250313e-16
brents-method         10 1.324717957244746 2.220446049250313e-16
brent-zero-generator  10 1.324717957244746 2.220446049250313e-16

21: x=>Math.PI+5*Math.sin(x/2)-x     in [0, 6.3]
     f(0) = 3.141592653589793    f(6.3) = -3.2004435832459497
burkardt.js           10 5.362613660752018 -1.7763568394002505e-15
netlib.js             10 5.362613660752018 -1.7763568394002505e-15
recipes.js            10 5.362613660752018 -1.7763568394002505e-15
wikipedia.js          53 5.362613660752017 8.881784197001252e-16
brents-method        202 NaN NaN
brent-zero-generator  10 5.362613660752018 -1.7763568394002505e-15

22: x=>(2-Math.exp(-x)+x*x)/3-x     in [-5, 5]
     f(-5) = -35.4710530341922    f(5) = 3.9977540176669724
burkardt.js           12 2.109356995571016 0
netlib.js             15 -2.992234872053937 4.440892098500626e-16
recipes.js            12 2.109356995571016 0
wikipedia.js          12 2.1093569955710154 0
brents-method        202 NaN NaN
brent-zero-generator  12 2.109356995571016 0

23: x=>5/(x*x)+2-x     in [1, 5]
     f(1) = 6    f(5) = -2.8
burkardt.js            9 2.690647448028614 -4.440892098500626e-16
netlib.js             10 2.6906474480286136 4.440892098500626e-16
recipes.js             9 2.690647448028614 -4.440892098500626e-16
wikipedia.js          53 2.6906474480286136 4.440892098500626e-16
brents-method        202 NaN NaN
brent-zero-generator   9 2.690647448028614 -4.440892098500626e-16

24: x=>Math.pow(5,-x)-x     in [-2, 5]
     f(-2) = 27    f(5) = -4.99968
burkardt.js            9 0.46962192293561056 0
netlib.js             10 0.46962192293561056 0
recipes.js             9 0.46962192293561056 0
wikipedia.js           9 0.46962192293561056 0
brents-method         28 0.4696219229356107 -3.3306690738754696e-16
brent-zero-generator   9 0.46962192293561056 0

25: x=>5*(Math.sin(x)+Math.cos(x))-x     in [-2, 1]
     f(-2) = -4.62722131686412    f(1) = 5.908866453380181
burkardt.js            9 -0.9151896213627041 -1.1102230246251565e-16
netlib.js              9 -0.9151896213627041 -1.1102230246251565e-16
recipes.js             9 -0.9151896213627041 -1.1102230246251565e-16
wikipedia.js          53 -0.9151896213627041 -1.1102230246251565e-16
brents-method         78 -0.915189621362704 3.3306690738754696e-16
brent-zero-generator   9 -0.9151896213627041 -1.1102230246251565e-16

26: x=>-x*x*x-Math.cos(x)     in [-3, 3]
     f(-3) = 27.989992496600447    f(3) = -26.010007503399553
burkardt.js           14 -0.8654740331016144 -1.1102230246251565e-16
netlib.js             14 -0.8654740331016144 -1.1102230246251565e-16
recipes.js            14 -0.8654740331016144 -1.1102230246251565e-16
wikipedia.js          53 -0.8654740331016144 -1.1102230246251565e-16
brents-method         90 -0.8654740331016144 -1.1102230246251565e-16
brent-zero-generator  14 -0.8654740331016144 -1.1102230246251565e-16

27: x=>(x-2)*x*x-5     in [1, 4]
     f(1) = -6    f(4) = 27
burkardt.js           11 2.6906474480286136 -1.7763568394002505e-15
netlib.js             11 2.6906474480286136 -1.7763568394002505e-15
recipes.js            11 2.6906474480286136 -1.7763568394002505e-15
wikipedia.js          53 2.6906474480286136 -1.7763568394002505e-15
brents-method        202 NaN NaN
brent-zero-generator  11 2.6906474480286136 -1.7763568394002505e-15

28: x=>(x+3)*x*x-1     in [-3, -2]
     f(-3) = -1    f(-2) = 3
burkardt.js            9 -2.879385241571817 -6.661338147750939e-16
netlib.js              9 -2.879385241571817 -6.661338147750939e-16
recipes.js             9 -2.879385241571817 -6.661338147750939e-16
wikipedia.js          53 -2.879385241571817 -6.661338147750939e-16
brents-method        202 NaN NaN
brent-zero-generator   9 -2.879385241571817 -6.661338147750939e-16

29: x=>x-Math.cos(x)     in [0, 1.6]
     f(0) = -1    f(1.6) = 1.629199522301289
burkardt.js            8 0.7390851332151607 0
netlib.js              8 0.7390851332151607 0
recipes.js             8 0.7390851332151607 0
wikipedia.js           8 0.7390851332151607 0
brents-method         27 0.7390851332151607 0
brent-zero-generator   8 0.7390851332151607 0

30: x=>Math.exp(x)+Math.pow(2,-x)+2*Math.cos(x)-6     in [1, 2]
     f(1) = -1.701113559804675    f(2) = 0.8067624258363653
burkardt.js            9 1.829383601933849 8.881784197001252e-16
netlib.js              9 1.829383601933849 8.881784197001252e-16
recipes.js             9 1.829383601933849 8.881784197001252e-16
wikipedia.js           9 1.8293836019338487 0
brents-method         92 1.8293836019338487 0
brent-zero-generator   9 1.829383601933849 8.881784197001252e-16

31: x=>Math.log(x-1)+Math.cos(x-1)     in [1.3, 2]
     f(1.3) = -0.24863631520032992    f(2) = 0.5403023058681398
burkardt.js           10 1.397748475958747 2.220446049250313e-16
netlib.js             10 1.397748475958747 2.220446049250313e-16
recipes.js            10 1.397748475958747 2.220446049250313e-16
wikipedia.js          53 1.397748475958747 2.220446049250313e-16
brents-method         19 1.397748475958747 2.220446049250313e-16
brent-zero-generator  10 1.397748475958747 2.220446049250313e-16

32: x=>2*x*Math.cos(2*x)-Math.pow(x-2,2)     in [2, 3]
     f(2) = -2.6145744834544478    f(3) = 4.761021719902196
burkardt.js            9 2.370686917662262 1.7208456881689926e-15
netlib.js             10 2.370686917662262 1.7208456881689926e-15
recipes.js             9 2.370686917662262 1.7208456881689926e-15
wikipedia.js          53 2.370686917662262 1.7208456881689926e-15
brents-method        202 NaN NaN
brent-zero-generator   9 2.370686917662262 1.7208456881689926e-15

33: x=>Math.exp(x)-3*x*x     in [3, 5]
     f(3) = -6.914463076812332    f(5) = 73.4131591025766
burkardt.js           12 3.7330790286328144 7.105427357601002e-15
netlib.js             12 3.7330790286328144 7.105427357601002e-15
recipes.js            12 3.7330790286328144 7.105427357601002e-15
wikipedia.js          53 3.733079028632814 -7.105427357601002e-15
brents-method        202 NaN NaN
brent-zero-generator  12 3.7330790286328144 7.105427357601002e-15

34: x=>Math.sin(x)-1/Math.exp(x)     in [0, 1]
     f(0) = -1    f(1) = 0.47359154363645417
burkardt.js            8 0.5885327439818611 0
netlib.js              8 0.5885327439818611 0
recipes.js             8 0.5885327439818611 0
wikipedia.js           8 0.5885327439818611 0
brents-method         23 0.5885327439818608 -3.3306690738754696e-16
brent-zero-generator   8 0.5885327439818611 0

35: x=>3*x-Math.exp(x)     in [1, 2]
     f(1) = 0.2817181715409549    f(2) = -1.3890560989306504
burkardt.js           10 1.5121345516578426 0
netlib.js             10 1.5121345516578426 0
recipes.js            10 1.5121345516578426 0
wikipedia.js          10 1.5121345516578426 0
brents-method         13 1.5121345516578422 0
brent-zero-generator  10 1.5121345516578426 0

36: x=>x+3*Math.cos(x)-Math.exp(x)     in [0, 1]
     f(0) = 2    f(1) = -0.0973749108546258
burkardt.js            8 0.9767730170494437 -4.440892098500626e-16
netlib.js              8 0.9767730170494437 -4.440892098500626e-16
recipes.js             8 0.9767730170494437 -4.440892098500626e-16
wikipedia.js           8 0.9767730170494436 0
brents-method         18 0.9767730170494436 0
brent-zero-generator   8 0.9767730170494437 -4.440892098500626e-16

37: x=>x*x-4*x+4-Math.log(x)     in [1, 2]
     f(1) = 1    f(2) = -0.6931471805599453
burkardt.js           10 1.4123911720238846 -1.1102230246251565e-16
netlib.js             10 1.4123911720238844 0
recipes.js            10 1.4123911720238846 -1.1102230246251565e-16
wikipedia.js          10 1.4123911720238844 0
brents-method         19 1.4123911720238844 0
brent-zero-generator  10 1.4123911720238846 -1.1102230246251565e-16

38: x=>x+1-2*Math.sin(Math.PI*x)     in [0, 0.5]
     f(0) = 1    f(0.5) = -0.5
burkardt.js           10 0.20603511957096587 0
netlib.js             10 0.20603511957096587 0
recipes.js            10 0.20603511957096587 0
wikipedia.js          12 0.20603511957096587 0
brents-method         19 0.20603511957096587 0
brent-zero-generator  10 0.20603511957096587 0

39: x=>x+1-2*Math.sin(Math.PI*x)     in [0.5, 1]
     f(0.5) = -0.5    f(1) = 1.9999999999999998
burkardt.js           11 0.6819748087386476 4.440892098500626e-16
netlib.js             11 0.6819748087386476 4.440892098500626e-16
recipes.js            11 0.6819748087386476 4.440892098500626e-16
wikipedia.js          10 0.6819748087386475 0
brents-method          9 0.6819748087386475 0
brent-zero-generator  11 0.6819748087386476 4.440892098500626e-16

40: x=>Math.pow(x, 3) - 2*x - 5     in [2, 3]
     f(2) = -1    f(3) = 16
burkardt.js            8 2.094551481542327 3.552713678800501e-15
netlib.js             10 2.0945514815423265 -8.881784197001252e-16
recipes.js             8 2.094551481542327 3.552713678800501e-15
wikipedia.js          53 2.0945514815423265 -8.881784197001252e-16
brents-method        202 NaN NaN
brent-zero-generator   8 2.094551481542327 3.552713678800501e-15
Exact root is 		2.0945514815

41: x=>Math.cos(x)-x     in [2, 3]
     f(2) = -2.4161468365471426    f(3) = -3.989992496600445
burkardt.js           51 2 -2.4161468365471426
netlib.js             51 2 -2.4161468365471426
recipes.js            51 2 -2.4161468365471426
wikipedia.js          53 2 -2.4161468365471426
brents-method         54 2 -2.4161468365471426
brent-zero-generator  51 2 -2.4161468365471426

42: x=>Math.cos(x)-x     in [-1, 3]
     f(-1) = 1.5403023058681398    f(3) = -3.989992496600445
burkardt.js           10 0.7390851332151607 0
netlib.js             10 0.7390851332151607 0
recipes.js            10 0.7390851332151607 0
wikipedia.js          12 0.7390851332151607 0
brents-method          9 0.7390851332151606 1.1102230246251565e-16
brent-zero-generator  10 0.7390851332151607 0

43: x=>Math.sin(x)-x     in [-1, 3]
     f(-1) = 0.1585290151921035    f(3) = -2.8588799919401326
burkardt.js           79 -1.8786573356225315e-8 0
netlib.js             57 -1.643737357379081e-8 0
recipes.js            79 -1.8786573356225315e-8 0
wikipedia.js          53 -7.4838406581998e-8 6.617444900424221e-23
brents-method          4 0 0
brent-zero-generator  79 -1.8786573356225315e-8 0

44: x=>(x+3)/Math.pow(x-1,2)     in [-4, 1.3333333333333333]
     f(-4) = -0.04    f(1.3333333333333333) = 39.000000000000014
burkardt.js           11 -3 0
netlib.js             13 -3 0
recipes.js            11 -3 0
wikipedia.js          11 -3 0
brents-method         22 -3.0000000000000058 -3.6082248300317494e-16
brent-zero-generator  11 -3 0

45: x =>x**9     in [-1, 1.1]
     f(-1) = -1    f(1.1) = 2.357947691000002
burkardt.js          332 3.275893473762236e-37 0
netlib.js            248 1.0011321899053399e-36 0
recipes.js           102 -1.1909327083614282e-11 -4.819311607999226e-99
wikipedia.js          53 6.051634374475811e-9 1.0885644065617109e-74
brents-method          9 -0.015625000002457232 -5.551115130982641e-17
brent-zero-generator 332 3.275893473762236e-37 0

46: x =>x**9     in [-1, 4]
     f(-1) = -1    f(4) = 262144
burkardt.js          341 -3.3496245851263877e-37 -0
netlib.js            249 4.68338210929683e-37 0
recipes.js           102 -3.8387285266449143e-11 -1.810033449114673e-94
wikipedia.js          53 -1.916440619349752e-8 -3.487051048020218e-70
brents-method         12 0.01561904255362994 5.5320955573324094e-17
brent-zero-generator 341 -3.3496245851263877e-37 -0

47: x =>x**19     in [-1, 4]
     f(-1) = -1    f(4) = 274877906944
burkardt.js          168 6.915832622190997e-18 0
netlib.js            120 8.844518268263737e-18 0
recipes.js           102 -1.2893363837013663e-10 -1.2501396369390224e-188
wikipedia.js          53 -3.7190816889456565e-8 -6.890942651207273e-142
brents-method          8 -0.06250000000682121 -1.3234889828292945e-23
brent-zero-generator 168 6.915832622190997e-18 0

48: x => (Math.abs(x)<3.8e-4 ? 0 : x * Math.exp(-x*x))     in [-1, 4]
     f(-1) = -0.36787944117144233    f(4) = 4.5014069887703647e-7
burkardt.js            9 -9.462445702598105e-8 0
netlib.js              9 -9.462445702598105e-8 0
recipes.js             9 -9.462445702598105e-8 0
wikipedia.js           9 -0.000011110034718112439 0
brents-method         13 -0.00002412285260155936 0
brent-zero-generator   9 -9.462445702598105e-8 0

49: x => (x+3)*Math.pow(x-1,2)     in [-4, 1.3333333333333333]
     f(-4) = -25    f(1.3333333333333333) = 0.48148148148148123
burkardt.js           13 -3 0
netlib.js             12 -3 0
recipes.js            13 -3 0
wikipedia.js          13 -3 0
brents-method          6 -3 0
brent-zero-generator  13 -3 0
```
