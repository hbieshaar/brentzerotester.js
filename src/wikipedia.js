/**
 * @see https://en.wikipedia.org/wiki/Talk:Brent%27s_method
 * on the tab 'Talk'
 */

/**
 * 
 * @param {number} a 
 * @param {number} b 
 * @param {(x:number):number} f 
 * @param {number} errorTol 
 * @param {number} maxiter 
 * @returns 
 */
export function brent_zero(a, b, f, errorTol, maxiter)
{
   let fa = f(a);
   let fb = f(b);
   
   // // if f(a) f(b) >= 0 then error-exit
   // if (fa * fb >= 0)
   //    return (Math.abs(fa) < Math.abs(fb) ? a : b);
   
   // if |f(a)| < |f(b)| then swap (a,b) end if
   if (Math.abs(fa) < Math.abs(fb)) {
      let tmp = a; a = b; b = tmp;
      tmp = fa; fa = fb; fb = tmp;
   }
   
   let c = 0;
   let d = Number.MAX_VALUE;
   
   let fc = 0;
   let s = 0;
   let fs = 0;
   
   c = a;
   fc = fa;
   let mflag = 1;
   let i = 0;
   
   while (1)
   {
      if (fb==0)
         return b;

      if (Math.abs(a-b) < errorTol)
         return b;
   
      if ((fa != fc) && (fb != fc))
        // Inverse quadratic interpolation
        s = a * fb * fc / (fa - fb) / (fa - fc) + b * fa * fc / (fb - fa) /
        (fb - fc) + c * fa * fb / (fc - fa) / (fc - fb);
      else
        // Secant Rule
        s = b - fb * (b - a) / (fb - fa);
      
      let tmp2 = (3 * a + b) / 4;
      if ((!(((s > tmp2) && (s < b)) || ((s < tmp2) && (s > b)))) ||
          (mflag && (Math.abs(s - b) >= (Math.abs(b - c) / 2))) ||
          (!mflag && (Math.abs(s - b) >= (Math.abs(c - d) / 2))))
      {
         s = (a + b) / 2;
         mflag = 1;
      }
      else
      {
         if ((mflag && (Math.abs(b - c) < errorTol)) ||
             (!mflag && (Math.abs(c - d) < errorTol)))
         {
            s = (a + b) / 2;
            mflag = 1;
         }
         else
           mflag = 0;
      }
      fs = f(s);
      d = c;
      c = b;
      fc = fb;
      if (fa * fs < 0) { b = s; fb = fs; }
      else { a = s; fa = fs; }
      
      // if |f(a)| < |f(b)| then swap (a,b) end if
      if (Math.abs(fa) < Math.abs(fb))
      { let tmp = a; a = b; b = tmp; tmp = fa; fa = fb; fb = tmp; }
      i++;
      if (i > maxiter)
      {
         // printf("Error is %f \n", fb);
         break;
      }
   }
   
   // printf("Number of iterations : %d\n",i);
   return b;
}
