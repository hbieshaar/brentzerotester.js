/**
 * @see 'Numerial Recipes (3rd Ed.)'
 * Using Brent’s method, return the root of a function or functor f
 * known to lie between x1 and x2. The root will be refined until its
 * accuracy is tol.
 */

/**
 * 
 * @param {(x:number):number} f 
 * @param {number} x1 
 * @param {number} x2 
 * @param {number} tol 
 * @param {number} max_iter 
 * @returns 
 */
export function zbrent(x1, x2, f, tol, max_iter)
{
   const ITMAX = 100;    // Maximum allowed number of iterations.
   let a=x1, b=x2, c=x2, d, e, fa=f(a);
   let fb = f(b);
   let fc, p, q, r, s, tol1, xm;

   // if ((fa > 0.0 && fb > 0.0) || (fa < 0.0 && fb < 0.0))
   //    return (Math.abs(a) < Math.abs(b) ? a : b);

   fc = fb;
   for (let iter=0; iter<ITMAX; iter++) {
      if ((fb > 0.0 && fc > 0.0) || (fb < 0.0 && fc < 0.0)) {
         // Rename a, b, c and adjust bounding interval
         c=a;
         fc=fa;
         e=d=b-a;
      }
      if (Math.abs(fc) < Math.abs(fb)) {
         a=b;
         b=c;
         c=a;
         fa=fb;
         fb=fc;
         fc=fa;
      }
      tol1 = 2.0 * Number.EPSILON * Math.abs(b) + 0.5 * tol;     // Convergence check.
      xm=0.5*(c-b);
      if (Math.abs(xm) <= tol1 || fb == 0.0) return b;
      if (Math.abs(e) >= tol1 && Math.abs(fa) > Math.abs(fb)) {
         s=fb/fa;       // Attempt inverse quadratic interpolation.
         if (a == c) {
            p=2.0*xm*s;
            q=1.0-s;
         } else {
            q=fa/fc;
            r=fb/fc;
            p=s*(2.0*xm*q*(q-r)-(b-a)*(r-1.0));
            q=(q-1.0)*(r-1.0)*(s-1.0);
         }
         if (p > 0.0) q = -q;    // Check whether in bounds.
         p=Math.abs(p);
         let min1=3.0*xm*q-Math.abs(tol1*q);
         let min2=Math.abs(e*q);
         if (2.0*p < (min1 < min2 ? min1 : min2)) {
         e=d;     // Accept interpolation.
         d=p/q;
         } else {
         d=xm;       // Interpolation failed, use bisection.
         e=d;
         }
      } else { // Bounds decreasing too slowly, use bisection.
      d=xm;
      e=d;
      }
      a=b;     // Move last best guess to a.
      fa=fb;
      if (Math.abs(d) > tol1) // Evaluate new trial root.
      b += d;
      else
      b += (xm < 0 ? -1 : 1) * Math.abs(tol1);
      fb=f(b);
      }
   //throw ("Maximum number of iterations exceeded in zbrent");
   return b;
}