// converted C language files
import { zeroin as netlib } from './src/netlib.js';
import { zbrent as recipes } from './src/recipes.js';
import { zero as burkardt } from './src/burkardt.js';
import { brent_zero as wikipedia } from './src/wikipedia.js';

// import npm packages
import LimeCoco from 'brents-method';
import Brent from 'brent-zero-generator';

const show_iter = false;

let example = 0;    /* case counter */
let counts;
function clearCounts() {
    counts = {
        all: 0,
        'bi ': 0,
        sec: 0,
        iqi: 0,
    }
}

function count(f) {
    return function (x, type='   ') {
        counts.all++;
        if (type in counts) counts[type]++;

        const fx = f(x);
        show_iter && console.log(`${counts.all}: (${type}) f(${x}) = ${fx}`)
        return fx;
    }
}

/**
 * 
 * @param {string} id 
 * @param {number} x 
 * @param {(x:number)=>number} f 
 */
function report(id, x, f) {
    if ( typeof x !== 'number' ) x = NaN;

    // console.log(`${id}${counts.all} ${x} ${f(x)}`);
    console.log(id.padEnd(20), counts.all.toString().padStart(3), x, f(x));
}

/**
 * Run a test
 * @param {number} a 
 * @param {number} b 
 * @param {(x:number)=>number} f 
 */
function test(a, b, f) {

    const tol = 1.0E-100;

    // const tol = 0;
    const msg = f.toString();

    clearCounts();
    f = count(f);

    let root;

    console.log(`\n${++example}: ${msg}     in [${a}, ${b}]\n     f(${a}) = ${f(a)}    f(${b}) = ${f(b)}`);

    clearCounts();
    root = burkardt(a, b, Number.EPSILON, tol, f);
    report('burkardt.js', root, f);

    clearCounts();
    root = netlib(a, b, f, tol);
    report('netlib.js', root, f);

    clearCounts();
    root = recipes(a, b, f, tol, 50);
    report('recipes.js', root, f);

    clearCounts();
    root = wikipedia(a, b, f, tol, 50);
    report('wikipedia.js', root, f);

    clearCounts();
    root = LimeCoco(f, a, b, {errorTolerance: tol, maxIterations:200});
    report('brents-method', root, f);

    clearCounts();
    root = Brent.zero(f, a, b, Number.EPSILON, tol);
    report('brent-zero-generator', root, f);

}

/**
 * funnies are the tests for intervals that have f(a)f(b) > 0;
 */
const funnies = false;

function main()
{
    test(0.0, 1.0, x=>Math.sqrt(x)-Math.cos(x));
    funnies && test(-2.0, 2.0, x=>3*(x+1)*(x-5)*(x-1));
    funnies && test(0.8, 2.5, x=>3*(x+1)*(x-5)*(x-1));
    test(0.0, 1.0, x=>Math.pow(x,3)-7*Math.pow(x,2)+14*x-6);
    test(3.2, 4.0, x=>Math.pow(x,3)-7*Math.pow(x,2)+14*x-6);
    test(-2.0, -1.0, x=>Math.pow(x,4)-2*Math.pow(x,3)-4*Math.pow(x,2)+4*x+4);
    test(0.0, 2.0, x=>Math.pow(x,4)-2*Math.pow(x,3)-4*Math.pow(x,2)+4*x+4);
    test(0.0, 1.0, x=>x - 2 * (-x));
    test(0.0, 1.0, x=>Math.exp(x) + (-x+3)*x-2);
    funnies && test(-0.5, 2.0, x=>2*Math.cos(2*x) - (x+1)*(x+1));
    test(-1.0, 0.0, x=>2*Math.cos(2*x) - (x+1)*(x+1));
    funnies && test(-1.0, 2.0, x=>3*x+Math.exp(x));
    test(0.0, 1.0, x=>x+3*Math.cos(x)-Math.exp(x));
    test(1.0, 2.0, x=>x*x-4*x+4-Math.log(x));
    test(2.0, 4.0, x=>x*x-4*x+4-Math.log(x));
    test(0.0, 0.5, x=>x + 1 - 2*Math.sin(Math.PI*x));
    test(0.5, 1.0, x=>x + 1 - 2*Math.sin(Math.PI*x));
    test(-1.0, 2.0, x=>Math.exp(x) -2 -Math.cos(Math.exp(x)-2));
    test(-0.5, 2.4, x=>(x+2)*Math.pow(x+1,2)*x*Math.pow(x-1,3)*(x-2));
    test(-0.5, 3.0, x=>(x+2)*Math.pow(x+1,2)*x*Math.pow(x-1,3)*(x-2));
    test(-3.0, -0.5, x=>(x+2)*Math.pow(x+1,2)*x*Math.pow(x-1,3)*(x-2));
    test(-1.5, 1.8, x=>(x+2)*(x+1)*x*Math.pow(x-1,3)*(x-2));
    test(1.0, 2.0, x=>Math.pow(x,4)-3*Math.pow(x,2)-3);
    test(1.0, 2.0, x=>Math.pow(x,3)-x-1);
    test(0.0, 6.3, x=>Math.PI+5*Math.sin(x/2)-x);
    funnies && test(0.3, 1.5, x=>Math.pow(2,x)-2*x);
    test(-5.0, 5.0, x=>(2-Math.exp(-x)+x*x)/3-x);
    test(1.0, 5.0, x=>5/(x*x)+2-x);
    funnies && test(2.0, 4.0, x=>Math.sqrt(Math.exp(x)/3)-x);
    test(-2.0, 5.0, x=>Math.pow(5,-x)-x);
    test(-2.0, 1.0, x=>5*(Math.sin(x)+Math.cos(x))-x);
    test(-3.0, 3.0, x=>-x*x*x-Math.cos(x));
    test(1.0, 4.0, x=>(x-2)*x*x-5);
    test(-3.0, -2.0, x=>(x+3)*x*x-1);
    test(0.0, 1.6, x=>x-Math.cos(x));
    funnies && test(0.0, 1.6, x=>x-8-2*Math.sin(x));
    test(1.0, 2.0, x=>Math.exp(x)+Math.pow(2,-x)+2*Math.cos(x)-6);
    test(1.3, 2.0, x=>Math.log(x-1)+Math.cos(x-1));
    test(2.0, 3.0, x=>2*x*Math.cos(2*x)-Math.pow(x-2,2));
    test(3.0, 5.0, x=>Math.exp(x)-3*x*x);
    test(0.0, 1.0, x=>Math.sin(x)-1/Math.exp(x));
    test(1.0, 2.0, x=>3*x-Math.exp(x));
    test(0.0, 1.0, x=>x+3*Math.cos(x)-Math.exp(x));
    test(1.0, 2.0, x=>x*x-4*x+4-Math.log(x));
    test(0.0, 0.5, x=>x+1-2*Math.sin(Math.PI*x));
    test(0.5, 1.0, x=>x+1-2*Math.sin(Math.PI*x));

    test(2.0, 3.0, x=>Math.pow(x, 3) - 2*x - 5);
    console.log("Exact root is \t\t2.0945514815");

    test(2.0, 3.0, x=>Math.cos(x)-x);
    test(-1.0, 3.0, x=>Math.cos(x)-x);
    test(-1.0, 3.0, x=>Math.sin(x)-x);

    // from tab talk on https://en.wikipedia.org/wiki/Brent%27s_method
    test(-4.0, 4.0 / 3.0, x=>(x+3)/Math.pow(x-1,2));

    // Brent's own tests
    test(-1.0, 1.1, x =>x**9);
    test(-1.0, 4, x =>x**9);
    test(-1.0, 4, x =>x**19);
    test(-1, 4, x => (Math.abs(x)<3.8e-4 ? 0 : x * Math.exp(-x*x)));
    // test(-1001200, 0, x => (x > 11e<3.8e-4 ? 0 : x * Math.exp(-x*x)));

    // LimeCoco example
    test(-4, 4/3, x => (x+3)*Math.pow(x-1,2));
}

main();
